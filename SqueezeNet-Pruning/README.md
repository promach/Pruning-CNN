# Pruning-CNN

PyTorch implementation of [1611.06440 Pruning Convolutional Neural Networks for Resource Efficient Inference]

This part is modified for "SqueezeNet" from the https://github.com/jacobgil/pytorch-pruning and https://github.com/Kuldeep-Attri/Pruning-CNN/tree/master/SqueezeNet-Pruning

At each pruning step 128 filters are removed from the network.

# Usage

This repository uses the PyTorch ImageFolder loader, so it assumes that the images are in a different directory for each category.

Train

......... Lemon

......... Orange

Test

......... Lemon

......... Orange


Training: python finetune.py --train

Pruning: python finetune.py --prune

Testing: python predict.py --image [IMAGE_FILENAME](https://gitlab.com/promach/Pruning-CNN/blob/master/SqueezeNet-Pruning/predict.py#L72) --model model_prunned --num_class NUMBER_OF_DIFFERENT_CLASSES_OF_OBJECTS  (remember to modify class_mapping.txt accordingly for your own dataset)

Calculating Flops: python finetune.py --flops (change the model name in finetune.py)

For first-time user or instruction on how to use this repo code, please have a look at [this colab example](https://colab.research.google.com/drive/1AK3O0YFt8rNTpdiBejarGFomkstbt_hP)